var gulp = require("gulp");
var sass = require("gulp-sass");
var csso = require('gulp-csso');

gulp.task("css", function () {
  gulp.src("scss/style.scss")
  .pipe(sass())
  .pipe(csso())
    .pipe(gulp.dest('dist'));
});

gulp.task("watch", function () {
  gulp.watch("scss/*", ["css"]);

});



gulp.task("default", [ "watch"]);
